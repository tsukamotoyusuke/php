<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {
    protected $fillable = ['title', 'body', 'published_at'];

    protected $dates = [
    'published_at'
  ];

  public function comment() {
    return $this->belogsTo('App\Article');
  }
}
