<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
  public function index() //1
    {
        return view('welcome');
    }

    public function contact()  //2
    {
        //return "contact";  // (a) view 関数を使わず、テキストを返してみる
        //↓へ変更
        return view("contact");  // (a) view 関数に変更
    }
}
