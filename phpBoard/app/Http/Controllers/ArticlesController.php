<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Article;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use Carbon\Carbon;

class ArticlesController extends Controller
{
  public function index() {
    //全件取得から最新の記事から順に表示されるよう変更20
    //$articles = Article::all();
    $articles = Article::latest('published_at')->latest('created_at')->published()->get();
    $comments = Comment::all();

    return view('articles.index', compact('articles') ,compact('comments'));
  }
 
  public function show($id) {
    $article = Article::findOrFail($id);
    return view('articles.show', compact('article'));
  }

  public function create() {
    return view('articles.create');
  }

  public function store(ArticleRequest $request) {
    
        Article::create($request->validated());
        //Comment::comment($request->validated());

        return redirect('articles');
  }

//21
  public function edit($id) {
    $article = Article::findOrFail($id);

    return view('articles.edit', compact('article'));
  }

  public function update(ArticleRequest $request, $id) {

    $article = Article::findOrFail($id);

    $article->update($request->validated());

    return redirect(url('articles', [$article->id]));
  }
//22
  public function destroy($id) {
    $article = Article::findOrFail($id);

    $article->delete();

    return redirect('articles')->with('message', '記事を削除しちゃいました。');
    
  }

  public function comments()
    {
        return view('articles.index');
    }

    
}
