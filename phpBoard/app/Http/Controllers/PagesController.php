<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
  public function about() {
      // 変数に値をセット //4
        $first_name = "Araddin";
        $last_name = "Rapunzel";
 
        // view関数の第２引数に compact関数を使う
        return view('pages.about', compact('first_name', 'last_name'));        
    
       // return view('pages.about'); //3
    }
}
