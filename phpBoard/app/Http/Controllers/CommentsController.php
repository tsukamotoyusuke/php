<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Controllers\Controller;
use App\Comment;

use Illuminate\Http\Request;

class CommentsController extends Controller
{
  public function comment() {
    return view('articles.comment');
    
  }

  public function store2() {
    $inputs = \Request::all();
    // ① マスアサインメントを使って、記事をDBに作成
    Comment::comment($inputs);
    // ② 記事一覧へリダイレクト
    return redirect('comments');
  }
}
