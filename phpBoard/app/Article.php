<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Article extends Model {
  
  protected $fillable = ['title', 'body', 'published_at'];

//20
  public function scopePublished($query) {
    $query->where('published_at', '<=', Carbon::now());
  }

  protected $dates = [
    'published_at'
  ];

  public function comments() {
    return $this->hasMany('App\Comment');
  }
}
