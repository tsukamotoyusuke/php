@extends('layout')
 
@section('content')
    <h1>タイトル"{{ $article->title }}"を編集するよ</h1>
 
    <hr/>
 
    @include('errors.form_errors')
 
    {!! Form::model($article, ['method' => 'PATCH', 'url' => ['articles', $article->id]]) !!}
        @include('articles.form', ['published_at' => $article->published_at->format('Y-m-d'), 'submitButton' => 'この編集で送信！！！'])
    {!! Form::close() !!}
 
@endsection