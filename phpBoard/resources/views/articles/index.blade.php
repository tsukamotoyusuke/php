@extends('layout')
 
@section('content')
    <h1>PHP掲示板投稿一覧画</h1>
 
    <hr/>
 
    @foreach($articles as $article)
        <article>
            <h2>
             投稿:   <a href="{{ url('articles', $article->id) }}">
                    {{ $article->title }}
                </a>
            </h2>
        </article>
        @foreach((array)$article->$comments as $comment)
             <comment>
                <h3>
                  コメント:  <a href="{{ url('articles', $comment->id) }}">
                            {{ $comment->title }}
                        </a>
                </h3>
            </comment>
        @endforeach
        <a href="articles/comment" class="btn btn-primary">コメント作成</a>
    @endforeach</br></br>
@endsection