@extends('layout')
 
@section('content')
    <h1>コメント</h1>
 
    <hr/>
 
    {!! Form::open(['url' => 'comments']) !!}
        <div class="form-group">
            {!! Form::label('title', 'コメントタイトル:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('body', 'コメント内容:') !!}
            {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('published_at', 'コメント公開日:') !!}
            {!! Form::input('date', 'published_at', date('Y-m-d'), ['class' => 'form-control']) !!}
        </div>    
        <div class="form-group">
            {!! Form::submit('このコメントで送信', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    {!! Form::close() !!}
@endsection