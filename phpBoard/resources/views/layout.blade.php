<!DOCTYPE HTML>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>PHPの掲示板だよ</title>

    <!-- ① CSS を追加 15-->
    <link rel="stylesheet" href="/css/app.css">
 
    <!-- ② JavaScript を追加 15-->
    <script src="/js/app.js" defer></script>
</head>
<body>

  {{-- ナビゲーションバーの Partial を使用 --}}
  @include('navbar')

  <div class="container py-4">
    {{-- フラッシュメッセージの表示 23--}}
    @if (session('message'))
    <div class="alert alert-success">{{ session('message') }}</div>
    @endif

    {{-- コンテンツの表示 --}}
    @yield('content')
  </div>
 
</body>
</html>