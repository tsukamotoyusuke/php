<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index'); //1
//Route::get('contact', 'WelcomeController@contact'); //2　
Route::get('about', 'PagesController@about'); //3

// root を記事一覧にします 26
Route::get('/', 'ArticlesController@index')->name('home');

//14
Route::get('articles', 'ArticlesController@index');

// 追加
Route::get('articles/create', 'ArticlesController@create'); //投稿入力

//コメント
Route::get('articles/comment', 'CommentsController@comment'); 
Route::post('comments', 'CommentsController@store2');

Route::post('articles', 'ArticlesController@store');//投稿をDBへ保存

Route::get('articles/{id}', 'ArticlesController@show');


Route::get('articles/{id}/edit', 'ArticlesController@edit');  // 21
Route::patch('articles/{id}', 'ArticlesController@update');  // 21
Route::delete('articles/{id}', 'ArticlesController@destroy');  // 22

