<!-- about.blade.php -->
 
<!DOCTYPE HTML>
<html>
<head>
    <title>About</title>
</head>
<body>

@extends('layout')
 
   {{-- ダブルカーリーを使うように修正 --}} 
@section('content')
    <h1>About Me: {{ $first_name }} {{ $last_name }}</h1>
    <h2>About Me: <?= $first_name ?> <?= $last_name ?></h2>
@endsection
</body>
</html>