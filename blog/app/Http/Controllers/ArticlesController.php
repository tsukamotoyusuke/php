<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
//use App\Http\Controllers\Controller
use App\Http\Requests\ArticleRequest;

use Carbon\Carbon;

class ArticlesController extends Controller {

  public function __construct()
    {
        $this->middleware('auth')
            ->except(['index', 'show']);
    }

  public function index() {
    
     // $articles = Article::all();  古いコード
     // $articles = Article::orderBy('published_at', 'desc')->orderBy('created_at', 'desc')->get();  これでもOKです
    $articles = Article::latest('published_at')->latest('created_at')
    //where('published_at', '<=', Carbon::now())
    ->published()
    ->get();
 
    return view('articles.index', compact('articles'));
  }

  public function create() {
    return view('articles.create');
  }

  public function show($id) {
    $article = Article::findOrFail($id);

    return view('articles.show', compact('article'));
  }

  public function store(ArticleRequest $request) {
  // ① フォームの入力値を取得
    //$inputs = \Request::all();
/*
    $rules = [    // ②
      'title' => 'required|min:3',
      'body' => 'required',
      'published_at' => 'required|date',
    ];
    $validated = $this->validate($request, $rules);  // ③
 
*/
    // ② デバッグ： $inputs の内容確認
    //dd($inputs);

    // ① マスアサインメントを使って、記事をDBに作成
    //Article::create($inputs);

    Article::create($request->validated());

    // ② 記事一覧へリダイレクト
    //return redirect('articles');
    return redirect()->route('articles.index')
            ->with('message', '記事を追加しました。');
  }


  public function edit($id) {
    $article = Article::findOrFail($id);

    return view('articles.edit', compact('article'));
  }

  public function update(ArticleRequest $request, $id) {

    $article = Article::findOrFail($id);

    $article->update($request->validated());

    //return redirect(url('articles', [$article->id]));
    return redirect()->route('articles.show', [$article->id])
            ->with('message', '記事を更新しました。');
  }


  public function destroy($id) {
    $article = Article::findOrFail($id);

    $article->delete();

    return redirect('articles')->with('message', '記事を削除しました。');
  }
}
