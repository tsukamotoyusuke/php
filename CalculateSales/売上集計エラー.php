<?php
# 変数名 -> スネークケース
# クラス名 -> アッパーキャメルケース
# 関数名 -> スネークケース

# 実行の仕方とコマンドライン引数の使用方法
# 1 : 実行方法
# php CalculateSales.php [FILE PATH]
# 2 : コマンドライン引数の使用
# $argv[1]

$branch_names = array();
$branch_sales = array();

# 支店定義ファイルの読み込み
$file = fopen($argv[1] . "/branch.lst", "r");
if($file) {
  while(($line = fgets($file)) !== false) {
    # explodeを使用し、支店定義ファイルの1行分のデータをカンマで分割する
    # 「rtrim」を使って行末のゴミ(改行コード)を削除している
    $data = explode(",", rtrim($line));
    $branch_names[$data[0]] = $data[1];
    $branch_sales[$data[0]] = 0;
  }
  fclose($file);
}

# 売上ファイルの抽出
$sale_files = array();
# コマンドライン引数で渡されたフォルダの中身を全て取得する
$dir = scandir($argv[1]);
foreach ($dir as $key => $val) {
  if(preg_match("/^[0-9]{8}\.rcd/", $val)) {
    # 数字8桁 + rcdの条件を満たすファイルを$sale_filesに格納する
    array_push($sale_files, $val);
  }
}

# 集計
foreach ($sale_files as $key => $val) {
  # 売上ファイルの数だけfopenでファイルを開き、集計していく
  $file = fopen($argv[1] . "/{$val}", "r");
  $rcd_data = array();
  if($file) {
    while(($line = fgets($file)) !== false) {
      # 売上ファイルの中身を$rcd_dataの中に格納していく
      # 「rtrim」を使って行末のゴミ(改行コード)を削除している
      array_push($rcd_data, rtrim($line));
    }
    fclose($file);
  }
  $branch_sales[$rcd_data[0]] = $branch_sales[$rcd_data[0]] + $rcd_data[1];
}

# 集計結果の出力
$file = fopen($argv[1] . "/branch.out", "w");
if($file) {
  foreach ($branch_names as $key => $val) {
    fwrite($file, "{$key},{$val},{$branch_sales[$key]}" . "\n");
  }
  fclose($file);
}

?>
