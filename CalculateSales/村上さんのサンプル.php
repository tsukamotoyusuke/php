<?php
# 実行の仕方とコマンドライン引数の使用方法
# 1 : 実行方法
# php CalculateSales.php [FILE PATH]
# 2 : コマンドライン引数の使用
# $argv[1]

$branch_names = array();
$branch_sales = array();

# 支店定義ファイルの読み込み
$file = fopen($argv[1] . "/branch.lst", "r");
if($file) {
  while(($line = fgets($file, 4096)) !== false) {
    echo $line . "\n";
  }
  fclose($file);
}

# 売上ファイルの抽出

# 連番チェック

# 集計

# 集計結果の出力

?>
