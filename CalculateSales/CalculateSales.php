<?php
# 変数名 -> スネークケース
# クラス名 -> アッパーキャメルケース
# 関数名 -> スネークケース
# 実行の仕方とコマンドライン引数の使用方法
# 1 : 実行方法
# php CalculateSales.php [FILE PATH]
# 2 : コマンドライン引数の使用
# $argv[1]

$branch_names = array();
$branch_sales = array();

# 支店定義ファイルの読み込み

//if( !file_exists("branch.lst") ) {
  //echo "支店定義ファイルが存在しません" . "\n";
//}

//fopen — ファイルまたは URL をオープンする
//"r":読み取り "w":書き出しのみでオープンします。
$file = fopen($argv[1] . "/branch.lst", "r");
if($file) {

  //fgetsで一行ずつ読み込む
  while( ($line = fgets($file, 4096)) !== false) {
    //↓で$lineの中身を","で区切ることができる
    $items = preg_split('/,/', rtrim($line));
    # 「rtrim」を使って行末のゴミ(改行コード)を削除している
    //↓()内の内容を出力
    // print_r($items);

    /*   実際の出力結果
    Array([0] => 001
          [1] => 札幌支店)
    001,札幌支店*/

    //要素は二つか/支店コードは0～9の3桁かどうか
   /* if ( (count($items) != 2) || !preg_match('/^[0-9]{3}/', $items[0]) ) {
      echo "支店定義ファイルがフォーマットが不正です" . "\n";
    }
    */
    
    $branch_names[$items[0]] = $items[1];
    
    //↑の具体例
    //$branchNames["001"] = "札幌支店";
    /* 実際の出力結果
    Array([001] => 札幌支店)*/

    /*自分で考えたコード
    $branchNames = $branchNames + array ($items[0] => $items[1]);
    (このコードでも間違いではないが
    $branchNames[$items[0]] = $items[1];
    と比べるとシンプルではない)*/
    
    $branch_sales[$items[0]] = 0;

    /*出力結果
        Array([001] => 0 [002] => 0 [003] => 0 [004] => 0 [005] => 0)*/
    //"\n"改行
     // echo $line . "\n";
   }
   fclose($file);
 }   
//print_r($branch_names);
//print_r($branch_sales);


# 売上ファイルの抽出

$rcd_files = array();

//filesの情報をいったん全部取ってくる
$path = $argv[1]; //or $path = './files/';
$files_all = scandir($path);

  foreach( $files_all as $key => $file_name ) {
    //↓のif文でrcdに該当するファイルかどうかの判定をする
    if( preg_match( '/^[0-9]{8}.rcd/', $file_name) ) {
      
       //array_push( 要素を追加する配列 , 追加したい要素);
        array_push($rcd_files, $file_name);
    }
  }
//print_r($rcd_files);"\n";


/*連番チェック
for ( $i = 0 ; $i < count($rcd_files) -1 ; $i++ ) {
//  if ( $rcd_files[$i+1] - $rcd_files[$i] != 1 ) {
    echo "a";
  //}
}
*/
# 集計

foreach ($rcd_files as $key => $file_name) {
  $file_contents = array();
  $sale_file = fopen($argv[1] . "/{$file_name}", "r");
  if($sale_file) {
    while( ($line2 = fgets($sale_file) ) !== false) {
      array_push($file_contents, rtrim($line2));
    }
    /*$sales = preg_split('/\R/', $line2);
     $fileContents[$sales[0]] = $sales[0];*/
   
     fclose($sale_file);
   }
   /*自分で考えたコード
   $saleFilesNum = intval($fileContents[1]);
   $branchSalesNum = intval($fileContents[0]);
   $saleAmmount = $saleFilesNum + $branchSalesNum;
   JAVAでは数字に変換していたがvalueの情報で
   $fileContents[0] : 支店コード
   $fileContents[1] : 売上
   なのでこの内容を上手く連結できればドンドン加算されていく*/

   $branch_sales[$file_contents[0]] = $branch_sales[$file_contents[0]] + $file_contents[1];

   //print_r($fileContents);
   //print_r($branch_sales);"\n";   
}

# 集計結果の出力

//$result = array();

$branch_out_file = fopen($argv[1] . "/branch.out", "w");
if($branch_out_file) {
  foreach ($branch_names as $key => $file_name ) {
   
   //fwiter($file, "支店コード,支店名,金額" . "\n");
    fwrite($branch_out_file, "$key, $file_name, $branch_sales[$key]" . "\n" );   
  }
  fclose($branch_out_file);
}
// $params = explode(",", $fileName);
    //$result[$params[0]] = $params[1];
   /* $allFile = array();
    array_push($allFile, $fileName);
    array_push($allFile, $branchSales);*/
//$code = array_keys($branchNames)
   //$money = array_values($branchSales[$key]);
   //$a =implode($money);
?>

